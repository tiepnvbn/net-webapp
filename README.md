## Overview

This desmonstrates how a standard .Net web application repository looks like and how the .Net web application pipeline works.
## Development Platforms

* .Net Framework 4.6.1
* MVC 5 web app
* NUnit

## Standard Repository Structure
A .Net web application should have 2 different repositories for 2 different workloads which are Infrastructure repository and Content repository. For simplifying this sample delivery, we combine those 2 repositories into one.

### Infrastructure Repository
This repository is used to maintain the infrastructure of the .Net web application. A standard infrastructure repository structure should look like as follows:

```txt
├── azuredeploy.json                    The ARM template that contains the web application infrastructure deployment configuration.<br>
├── azuredeploy.parameters.json         The ARM template parameters file.<br>
├── README.md                           The README file that describes purposes of the .Net web application as well as its needed guidances.
```
### Content Repository

This repository is used to maintain the content of the .Net web application. A standard content repository structure should look like as follows:
```text
└───GreetingService                             The root directory of the .Net web application.
    |   .gitignore                              The gitignore file of the .Net web application (optional).
    │   GreetingService.sln                     A solution is a structure for organizing projects in Visual Studio
    │
    ├───GreetingService                         The root directory where the .Net web application source code are located.
    │   │   favicon.ico                         Iconic image that represents your website
    │   │   Global.asax                         Is an optional file which is used to handling higher level application events such as Application_Start, Application_End, Session_Start, Session_End etc.
    │   │   Global.asax.cs                      Is an optional file which is used to handling higher level application events such as Application_Start, Application_End, Session_Start, Session_End etc.
    │   │   GreetingService.csproj              Is a Visual Studio .NET C# Project file extension. This file will have information about the files included in that project, assemblies used in that project, project GUID and project version etc. 
    │   │   GreetingService.csproj.user
    │   │   packages.config                     This allows NuGet to easily restore the project's dependencies when the project to be transported to a different machine, such as a build server
    │   │   Web.config                          Is an application configuration file of The Official Microsoft ASP.NET Site written in XML. It stays is the root directory of application and is responsible for controlling the application's behaviour.
    │   │
    │   ├───App_Start                           It contains various configurations files like as BundleConfig.cs, FilterConfig.cs, RouteConfig.cs, WebApiConfig.cs for your application. All these settings are registered within Application_Start method of Global.asax.cs file
    │   │       BundleConfig.cs                 Makes it easy to combine or bundle multiple files into a single file. You can create CSS, .NetScript and other bundles. 
    │   │       FilterConfig.cs                 Registered global filters. These filters are applied to all actions and controllers. 
    │   │       RouteConfig.cs                  Is responsible for mapping incoming browser request to unique controller action method. 
    │   │
    │   ├───Content                             The folder store images, stylesheet(.css file)
    │   ├───Controllers                         The folder contains controllers, API 
    │   │       ApiController.cs                The API controller
    │   │
    │   ├───fonts                               The folder store fonts
    │   ├───Models                              The folder of medels class
    │   ├───Properties                          The folder contain configurations as publish project info, Web app version
    │   │       AssemblyInfo.cs                 The class contain web app version, name...
    │   │
    │   ├───Scripts                             The folder contains scripts file, plugin
    │   └───Views
    │       │   Web.config                      To prevent access to your views by any means other than your controller
    │       │   _ViewStart.cshtml               Executes before the actual view code executes. By default, _ViewStart template is applied to all view files of this or any sub directory under this directory.
    │       │
    │       ├───Api                             
    │       │       Index.cshtml                View action it is result of action controller
    │       │
    │       └───Shared
    │               Error.cshtml                View default when error
    │               _Layout.cshtml              Default layout for web app
    │
    └───GreetingService.Tests                   The root directory where the script test are located.
        │   App.config                          Is an application configuration file of The Official Microsoft ASP.NET Site written in XML. It stays is the root directory of application and is responsible for controlling the application's behaviour.
        │   GreetingService.Tests.csproj        Is a Visual Studio .NET C# Project file extension. This file will have information about the files included in that project, assemblies used in that project, project GUID and project version etc. 
        │   packages.config                     This allows NuGet to easily restore the project's dependencies when the project to be transported to a different machine, such as a build server
        │
        ├───Controllers                         
        │       HomeControllerTest.cs           Code test for HomeController class
        │
        └───Properties
                AssemblyInfo.cs                 The class contain web app version, name...
```