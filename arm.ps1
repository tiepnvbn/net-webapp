﻿
$Proxy = "http://fsoft-proxy:8080"

[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12;
[system.net.webrequest]::defaultwebproxy = new-object system.net.webproxy($Proxy)
[system.net.webrequest]::defaultwebproxy.credentials = [System.Net.CredentialCache]::DefaultNetworkCredentials
[system.net.webrequest]::defaultwebproxy.BypassProxyOnLocal = $true


# Replace the following URL with a public GitHub repo URL
$gitrepo="https://gitlab.com/tiepnvbn/net-webapp.git"
$webappname="greetingservice$(Get-Random)"
$location="West US"
$myResourceGroup = "tiep-test-223"

# Create a resource group.
New-AzureRmResourceGroupDeployment -Name "rg1" -ResourceGroupName tiep-test-2213 -TemplateParameterFile azuredeploy.parameters.json -TemplateFile azuredeploy.json
# New-AzResourceGroup -Name $myResourceGroup -Location $location


