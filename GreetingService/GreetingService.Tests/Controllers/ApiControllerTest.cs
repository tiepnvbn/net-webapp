﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GreetingService;
using GreetingService.Controllers;
using System.Web.Helpers;
using GreetingService.Models;

namespace GreetingService.Tests.Controllers
{
    [TestClass]
    public class ApiControllerTest
    {
        [TestMethod]
        public void Index_Success_ReturnView()
        {
            // Arrange
            ApiController controller = new ApiController();

            // Act
            var result = controller.Index() as JsonResult;

            // Assert
            Assert.AreEqual(new object { }.ToString(), result.Data.ToString());
        }

        [TestMethod]
        public void GreetingService_Success_ReturnJsonResult()
        {
            var message = "testing_message";
            var messageModel = new MessageModel(message);
            
            // Arrange
            ApiController controller = new ApiController();
            
            // Act
            var result = controller.GreetingService(message) as JsonResult;
            var actualValue = result.Data as MessageModel;
            // Assert
            Assert.AreEqual(messageModel.Message, actualValue.Message);
             
        }
    }
}
