﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GreetingService.Models
{
    public class MessageModel
    {
        public MessageModel()
        {
        }

        public MessageModel(string message)
        {
            this.Message = message;
        }
        public string Message { get; set; }
    }
}