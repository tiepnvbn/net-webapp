﻿using GreetingService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GreetingService.Controllers
{
    public class ApiController : Controller
    {
        public ActionResult Index()
        {
            return Json(new object{ }, JsonRequestBehavior.AllowGet);
        }

        // GET: api/greetingservice
        [HttpGet]
        public ActionResult GreetingService(string message)
        {
            var messageModel = new MessageModel(message); 
            return Json(messageModel, JsonRequestBehavior.AllowGet);
        }
    }
}
