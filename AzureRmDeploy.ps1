﻿
$Proxy = "http://fsoft-proxy:8080"

[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12;
[system.net.webrequest]::defaultwebproxy = new-object system.net.webproxy($Proxy)
[system.net.webrequest]::defaultwebproxy.credentials = [System.Net.CredentialCache]::DefaultNetworkCredentials
[system.net.webrequest]::defaultwebproxy.BypassProxyOnLocal = $true


# Replace the following URL with a public GitHub repo URL
$gitrepo="https://gitlab.com/tiepnvbn/net-webapp.git"
$webappname="greetingservice$(Get-Random)"
$location="West US"
$myResourceGroup = "tiep-test-22"

# Create a resource group.
New-AzureRmDeployment -Name "rg1" -Location "West US" -TemplateParameterFile azuredeploy.parameters.json -TemplateFile azuredeploy.json
# New-AzResourceGroup -Name $myResourceGroup -Location $location

# Create an App Service plan in Free tier.

New-AzureRmAppServicePlan -Name $webappname -Location $location -ResourceGroupName $myResourceGroup -Tier Free

# Create a web app.
New-AzureRmWebApp -Name $webappname -Location $location -AppServicePlan $webappname -ResourceGroupName $myResourceGroup

# Configure GitHub deployment from your GitHub repo and deploy once.
$PropertiesObject = @{
    repoUrl = "$gitrepo";
    branch = "master";
    isManualIntegration = "true";
}


Set-AzureRmResource -PropertyObject $PropertiesObject -ResourceGroupName $myResourceGroup -ResourceType Microsoft.Web/sites/sourcecontrols -ResourceName $webappname/web -ApiVersion 2015-08-01 -Force
