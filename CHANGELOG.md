# RELEASE NOTES

This file is used to list changes made in each version of the sample .Net web application.

## 0.1.0

Initial release of the sample .Net web application.
